# frozen_string_literal: true

require "redis_client"

require "rspec/core/shared_context"

REDIS = RedisClient.new(url: REDIS_URL)

module RedisPrescriptionSharedContext
  extend RSpec::Core::SharedContext

  before do
    REDIS.call("SCRIPT", "FLUSH")
    REDIS.call("FLUSHDB")

    allow(REDIS).to receive(:call).and_call_original
  end

  def have_received_eval(script, keys: [], argv: [])
    have_received(:call).with("EVAL", script, keys.size, *keys, *argv).once
  end

  def have_received_evalsha(digest, keys: [], argv: [])
    have_received(:call).with("EVALSHA", digest, keys.size, *keys, *argv).once
  end

  def redis_set(key, value)
    REDIS.call("SET", key, value)
  end

  def redis_script_load(script)
    REDIS.call("SCRIPT", "LOAD", script)
  end
end
