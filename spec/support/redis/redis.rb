# frozen_string_literal: true

require "redis"

require "rspec/core/shared_context"

REDIS = Redis.new(url: REDIS_URL)

module RedisPrescriptionSharedContext
  extend RSpec::Core::SharedContext

  before do
    REDIS.script("FLUSH")
    REDIS.flushdb

    allow(REDIS).to receive(:eval).and_call_original
    allow(REDIS).to receive(:evalsha).and_call_original
  end

  def have_received_eval(script, keys: [], argv: [])
    have_received(:eval).with(script, keys, argv)
  end

  def have_received_evalsha(digest, keys: [], argv: [])
    have_received(:evalsha).with(digest, keys, argv)
  end

  def redis_set(key, value)
    REDIS.set(key, value)
  end

  def redis_script_load(script)
    REDIS.script("LOAD", script)
  end
end
