# frozen_string_literal: true

RSpec.describe Redis::Prescription do
  describe ".new" do
    context "when silence_deprecation_warning is false" do
      before { described_class.silence_deprecation_warning = false }

      it "prints deprecation warning" do
        expect { described_class.new("return 1") }
          .to output(%r{#{Regexp.escape described_class.name} usage was deprecated}).to_stderr
      end
    end

    context "when silence_deprecation_warning is true" do
      before { described_class.silence_deprecation_warning = true }

      it "suppresses deprecation warning" do
        expect { described_class.new("return 1") }
          .not_to output(%r{#{Regexp.escape described_class.name} usage was deprecated}).to_stderr
      end
    end
  end
end
