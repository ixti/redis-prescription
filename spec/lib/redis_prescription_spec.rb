# frozen_string_literal: true

RSpec.describe RedisPrescription do
  subject(:prescription) { described_class.new(script) }

  describe "#digest" do
    let(:script) { "return 42" }

    it "matches the digest Redis returns with SCRIPT LOAD" do
      expect(prescription.digest).to eq(redis_script_load(script))
    end
  end

  describe "#call" do
    let(:script) { "return redis.call('GET', KEYS[1]) * ARGV[1]" }

    before { redis_set("abc", 2) }

    it "uses EVAL once when script was not loaded yet" do
      3.times { prescription.call(REDIS, keys: ["abc"], argv: [123]) }

      aggregate_failures do
        expect(REDIS).to have_received_eval(prescription.source, keys: ["abc"], argv: [123]).once
        expect(REDIS).to have_received_evalsha(prescription.digest, keys: ["abc"], argv: [123]).thrice
      end
    end

    if "redis-client" == REDIS_GEM
      require "redis_client/decorator"

      context "with decorated redis-client" do
        before { stub_const("DecoratedRedis", RedisClient::Decorator.create(Module.new)) }

        it "uses EVAL once when script was not loaded yet" do
          3.times { prescription.call(DecoratedRedis.new(REDIS), keys: ["abc"], argv: [123]) }

          aggregate_failures do
            expect(REDIS).to have_received_eval(prescription.source, keys: ["abc"], argv: [123]).once
            expect(REDIS).to have_received_evalsha(prescription.digest, keys: ["abc"], argv: [123]).thrice
          end
        end
      end
    end

    it "executes Lua script" do
      expect(prescription.call(REDIS, keys: ["abc"], argv: [123])).to eq 246
    end

    context "when script execution fails" do
      let(:script) { "return 42 * 'question'" }

      it "raises RedisPrescription::ScriptError" do
        expect { prescription.call(REDIS) }.to raise_error(described_class::ScriptError)
      end
    end

    context "when script compilation fails" do
      let(:script) { "!!!" }

      it "raises RedisPrescription::ScriptError" do
        expect { prescription.call(REDIS) }.to raise_error(described_class::ScriptError)
      end
    end
  end
end
