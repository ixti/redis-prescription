# frozen_string_literal: true

RSpec.describe RedisPrescription::ScriptError do
  subject(:error) do
    error = nil

    begin
      RedisPrescription.new(script).call(REDIS)
    rescue => e
      error = e
    end

    error
  end

  context "when script execution fails" do
    let(:script) do
      <<~LUA
        local test

        function test ()
          return 2 * "test"
        end

        return test()
      LUA
    end

    it { is_expected.to be_a described_class }

    it "provides which line failure occured on" do
      expect(error.loc).to eq 4
    end

    it "provides source excerpt in the message" do
      expect(error.message).to include(<<~EXCERPT.rstrip)
        \t2 |
        \t3 | function test ()
        \t4 >   return 2 * "test"
        \t5 | end
        \t6 |
      EXCERPT
    end
  end

  context "when script compilation fails" do
    let(:script) do
      <<~LUA
        local test

        function test ()
          return 2 * 2
        end

        !!!
      LUA
    end

    it { is_expected.to be_a described_class }

    it "does not provides which line failure occured on" do
      expect(error.loc).to eq 7
    end

    it "provides source excerpt in the message" do
      expect(error.message).to include(<<~EXCERPT.rstrip)
        \t5 | end
        \t6 |
        \t7 > !!!
      EXCERPT
    end
  end
end
