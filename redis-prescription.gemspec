# frozen_string_literal: true

require_relative "./lib/redis_prescription/version"

Gem::Specification.new do |spec|
  spec.name          = "redis-prescription"
  spec.version       = RedisPrescription::VERSION
  spec.authors       = ["Alexey Zapparov"]
  spec.email         = ["alexey@zapparov.com"]

  spec.summary       = "Redis LUA stored procedure runner."
  spec.homepage      = "https://gitlab.com/ixti/redis-prescription"
  spec.license       = "MIT"

  spec.description   = <<~DESCRIPTION
    Preloads (and reloads when needed, e.g. when scripts were flushed away)
    script and then runs it with `EVALSHA`.
  DESCRIPTION

  spec.metadata["homepage_uri"]          = spec.homepage
  spec.metadata["source_code_uri"]       = "#{spec.homepage}/tree/v#{spec.version}"
  spec.metadata["bug_tracker_uri"]       = "#{spec.homepage}/issues"
  spec.metadata["changelog_uri"]         = "#{spec.homepage}/blob/v#{spec.version}/CHANGES.md"
  spec.metadata["rubygems_mfa_required"] = "true"

  spec.files = Dir.chdir(__dir__) do
    docs = %w[LICENSE.txt README.adoc].freeze

    `git ls-files -z`.split("\x0").select do |f|
      f.start_with?("lib/") || docs.include?(f)
    end
  end

  spec.bindir        = "exe"
  spec.executables   = spec.files.grep(%r{^exe/}) { |f| File.basename(f) }
  spec.require_paths = ["lib"]

  spec.required_ruby_version = ">= 2.7"
end
