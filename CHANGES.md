# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.1.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [2.6.0] - 2023-05-01

### Changed

- Refactor adapters into modules to avoid useless object allocation.


## [2.5.0] - 2023-04-10

### Fixed

- Ensure digest/sha1 is required


## [2.4.0] - 2023-04-08

### Added

- Bring back `Redis::Prescription` naming as an alias of `RedisPrescription`.
  Using it will output deprecation warning that can be disabled with:
  `Redis::Prescription.silence_deprecation_warning = true`.


## [2.3.0] - 2023-04-08

### Added

- Add official support of Ruby 3.2.x
- Bring back redis-rb 4.1.x..4.6.x support
- Bring back Ruby 2.7.x support


## [2.2.0] - 2023-04-06

### Added

- Bring back [redis-namespace](https://github.com/resque/redis-namespace) support.

### Removed

- Remove support of `ConnectionPool` or `RedisClient::Pooled` passed directly to
  `#call`.


## [2.1.0] - 2023-04-05

### Added

- Add failing line of code expose as `ScriptError#loc`.

### Changed

- Enhance Add source excerpt to `ScriptError#message`.

### Fixed

- Fix source exposed in `ScriptError`.


## [2.0.0] - 2023-04-05

### Added

- Add support of [redis-client](https://github.com/redis-rb/redis-client).

### Changed

- (BREAKING) Rename `Redis::Prescription` to `RedisPrescription`.
- (BREAKING) Rename `#eval` to `#call`.

### Removed

- (BREAKING) Remove `.read` syntax sugar.
- Drop support of Ruby < 3.0.
- Drop support of Redis Server < 6.2.
- Drop support of redis-namespace.
- Drop support of redis-rb < 4.7.


## [1.0.0] - 2018-02-11

### Added

- Initial release. Redis LUA script runner got extracted from sidekiq-throttled.

[unreleased]: https://gitlab.com/ixti/redis-prescription/-/compare/v2.5.0...main
[2.5.0]: https://gitlab.com/ixti/redis-prescription/-/compare/v2.4.0...v2.5.0
[2.4.0]: https://gitlab.com/ixti/redis-prescription/-/compare/v2.3.0...v2.4.0
[2.3.0]: https://gitlab.com/ixti/redis-prescription/-/compare/v2.2.0...v2.3.0
[2.2.0]: https://gitlab.com/ixti/redis-prescription/-/compare/v2.1.0...v2.2.0
[2.1.0]: https://gitlab.com/ixti/redis-prescription/-/compare/v2.0.0...v2.1.0
[2.0.0]: https://gitlab.com/ixti/redis-prescription/-/compare/v1.0.0...v2.0.0
[1.0.0]: https://gitlab.com/ixti/redis-prescription/-/commit/a313b4df2d2a328fc5f1e9861047fd65b45fefc2
